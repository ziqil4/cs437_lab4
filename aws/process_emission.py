import json
import logging
import sys
import pandas as pd
import platform
from threading import Timer

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

my_platform = platform.platform()

# Counter
def lambda_handler(event, context):
    global my_counter
    #TODO1: Get your data
    #TODO2: Calculate max CO2 emission
    filename = 'vehicle{}.csv'
    co2_map = {}
    for i in range(5):
        df = pd.read_csv(filename.format(i))
        max_co2 = max(df['vehicle_CO2'])
        co2_map[i] = max_co2

    #TODO3: Return the result

    try:
        if not my_platform:
            client.publish(
                topic="hello/world", queueFullPolicy="AllOrException", payload="Hello world! Sent from Greengrass Core."
            )
        else:
            client.publish(
                topic="iot/vehicle0",
                queueFullPolicy="AllOrException",
                payload=str(co2_map[0]),
            )
            client.publish(
                topic="iot/vehicle1",
                queueFullPolicy="AllOrException",
                payload=str(co2_map[1]),
            )
            client.publish(
                topic="iot/vehicle2",
                queueFullPolicy="AllOrException",
                payload=str(co2_map[2]),
            )
            client.publish(
                topic="iot/vehicle3",
                queueFullPolicy="AllOrException",
                payload=str(co2_map[3]),
            )
            client.publish(
                topic="iot/vehicle4",
                queueFullPolicy="AllOrException",
                payload=str(co2_map[4]),
            )
    except Exception as e:
        logger.error("Failed to publish message: " + repr(e))

    # Asynchronously schedule this function to be run again in 5 seconds
    Timer(5, lambda_handler).start()

lambda_handler()

def function_handler(event, context):
    return
################################################### Connecting to AWS
import boto3

import json
################################################### Create random name for things
import random
import string
import os

################################################### Parameters for Thing
thingArn = ''
thingId = ''
thingName = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(15)])
defaultPolicyName = 'GGTest_Group_Core-policy'

with open("thing_count.txt") as f:
    firstline = f.readline().rstrip()

total_things_count = int(firstline)
device_id = total_things_count
###################################################

def createThing():
  global thingClient
  thingResponse = thingClient.create_thing(
      thingName = thingName
  )
  data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
  for element in data: 
    if element == 'thingArn':
        thingArn = data['thingArn']
    elif element == 'thingId':
        thingId = data['thingId']
        createCertificate()


def createCertificate():
	global thingClient
	certResponse = thingClient.create_keys_and_certificate(
			setAsActive = True
	)
	data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
	for element in data: 
			if element == 'certificateArn':
					certificateArn = data['certificateArn']
			elif element == 'keyPair':
					PublicKey = data['keyPair']['PublicKey']
					PrivateKey = data['keyPair']['PrivateKey']
			elif element == 'certificatePem':
					certificatePem = data['certificatePem']
			elif element == 'certificateId':
					certificateId = data['certificateId']
							
	with open('public.pem', 'w') as outfile:
			outfile.write(PublicKey)
	with open('private.pem', 'w') as outfile:
			outfile.write(PrivateKey)
	with open('certificate.pem', 'w') as outfile:
			outfile.write(certificatePem)

	os.makedirs(f'certificates/device{device_id}')
	os.replace("public.pem", f"certificates/device{device_id}/public.pem")
	os.replace("private.pem", f"certificates/device{device_id}/private.pem")
	os.replace("certificate.pem", f"certificates/device{device_id}/certificate.pem")

	# response = thingClient.attach_policy(
	# 		policyName = defaultPolicyName,
	# 		target = certificateArn
	# )
	# response = thingClient.attach_thing_principal(
	# 	 	thingName = thingName,
	# 		principal = certificateArn
	# )

thingClient = boto3.client('iot')
createThing()

total_things_count += 1
with open("thing_count.txt", "w") as f:
    f.writelines(str(total_things_count) + '\n')
